# Новый проек

## Шаги

- создать папку проекта и инициализировать в нем git

    ```bash
    mkdir ml_football_predicter
    cd ml_football_predicter
    git init
    git config --local user.name pro100v
    git config --local user.email mr.deff@gmail.com
    ```

- создание проекта на git (lab\hub) и инициализация его

    ```bash
    git remote add origin https://gitlab.com/pro100v-pets/ml_football_predicter.git
    branch -M main
    git push -uf origin main
    ```

- поднимаем и настраиваем виртуальное окружение через poetry. На моменте инициализации отказываемся от интерактивного добавления зависимостей.

    ```bash
    poetry init
    poetry add pandas numpy click dvc["s3"] requests scikit-learn pyaml python-dotenv mlflow fastapi["all"] uvicorn pytest great-expectations
    poetry add -D mypy flake8 black pydocstyle flakeheaven flake8-commas flake8-quotes flake8-bandit flake8-docstrings
    ```

- инициализируем структуру проекта

    ```bash
    cookiecutter https://github.com/drivendata/cookiecutter-data-science
    ```
